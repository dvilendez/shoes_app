import 'package:flutter/material.dart';

class OrangeButton extends StatelessWidget {

  final String text;
  final double height;
  final double width;
  final Color color;

  const OrangeButton({
    @required this.text,
    this.height = 30,
    this.width = 150,
    this.color = Colors.orange
  });


  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: this.width,
      height: this.height,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(100),
        color: this.color,
      ),
      child: Text(this.text, style: TextStyle(color: Colors.white),),
      
    );
  }
}