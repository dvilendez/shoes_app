import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoes_app/src/models/shoe_model.dart';
import 'package:shoes_app/src/pages/shoe_desc_page.dart';

class ShoeSizePreview extends StatelessWidget {

  final bool fullScreen;

  const ShoeSizePreview({
    this.fullScreen = false
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (!this.fullScreen) {
          Navigator.push(context, MaterialPageRoute(builder: (BuildContext context) => ShoeDescPage()));
        }
      },
      child: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: (this.fullScreen) ? 5 : 30,
          vertical: (this.fullScreen) ? 0 : 5
        ),
        child: Container(
          width: double.infinity,
          height: (this.fullScreen) ? 410 : 430,
          decoration: BoxDecoration(
            color: Color(0xffFFCF53),
            borderRadius: 
              (!this.fullScreen) 
                ? BorderRadius.circular(50)
                : BorderRadius.only(bottomLeft: Radius.circular(50),
                                    bottomRight: Radius.circular(50),
                                    topLeft: Radius.circular(40),
                                    topRight: Radius.circular(40),)
          ),
          child: Column(
            children: <Widget>[
              // shoe with his shadow
              _ShoeWithShadow(),
              if (!this.fullScreen) _ShoeSizes()
            ],
          ),
        ),
      ),
    );
  }
}


class _ShoeSizes extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          _ShoeSizeBox(7),
          _ShoeSizeBox(7.5),
          _ShoeSizeBox(8),
          _ShoeSizeBox(8.5),
          _ShoeSizeBox(9),
          _ShoeSizeBox(9.5),
        ],
      ),
    );
  }
}

class _ShoeSizeBox extends StatelessWidget {

  final double number;

  const _ShoeSizeBox(this.number);


  @override
  Widget build(BuildContext context) {

    final shoeModel = Provider.of<ShoeModel>(context);

    return GestureDetector(
      onTap: () {
        Provider.of<ShoeModel>(context, listen: false).size = this.number;
      },
      child: Container(
        alignment: Alignment.center,
        child: Text(
          '${number.toString().replaceAll('.0', '')}',
          style: TextStyle(
            color: (this.number == shoeModel.size) ? Colors.white : Color(0xffF1A23A),
            fontSize: 16,
            fontWeight: FontWeight.bold
          )
        ),
        width: 45,
        height: 45,
        decoration: BoxDecoration(
          color: (this.number == shoeModel.size) ? Color(0xffF1A23A) : Colors.white,
          borderRadius: BorderRadius.circular(10),
          boxShadow: (this.number == shoeModel.size) 
                        ? [BoxShadow(
                          color: Color(0xffF1A23A),
                          blurRadius: 10,
                          offset: Offset(0, 5)
                        )]
                        : []
        ),
      ),
    );
  }
}



class _ShoeWithShadow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    final shoeModel = Provider.of<ShoeModel>(context);

    return Padding(
      padding: EdgeInsets.all(50),
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: 20,
            right: 0,
            child: _ShowShadow()
          ),
          Image(image: AssetImage(shoeModel.assetImage),)
        ],
      ),
    );
  }
}

class _ShowShadow extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Transform.rotate(
      angle: -0.5,
      child: Container(
        width: 230,
        height: 120,
        decoration: BoxDecoration(
          // color: Colors.red,
          borderRadius: BorderRadius.circular(100),
          boxShadow: [
            BoxShadow(color: Color(0xffEAA14E), blurRadius: 40 )
          ]
        ),
      ),
    );
  }
}