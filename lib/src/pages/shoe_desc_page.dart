
import 'package:animate_do/animate_do.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shoes_app/src/helpers/helpers.dart';
import 'package:shoes_app/src/models/shoe_model.dart';
import 'package:shoes_app/src/widget/custom_widgets.dart';
import 'package:shoes_app/src/widget/shoe_size.dart';

class ShoeDescPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    changeStatusLight();

    return Scaffold(
      body: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Hero(
                tag: 'shoe-1',
                child: ShoeSizePreview(fullScreen: true)
              ),
              Positioned(
                top: 80,
                child: FloatingActionButton(
                  child: Icon(Icons.chevron_left, color: Colors.white, size: 60,),
                  onPressed: () {
                    changeStatusDark();
                    Navigator.pop(context);
                  },
                  backgroundColor: Colors.transparent,
                  elevation: 0,
                  highlightElevation: 0,
                ),
              )
            ],
          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  ShoeDescription(
                    title: 'Nike Air Max 720',
                    description: "The Nike Air Max 720 goes bigger than ever before with Nike's taller Air unit yet, offering more air underfoot for unimaginable, all-day comfort. Has Air Max gone too far? We hope so."
                  ),
                  _AmmountBuyNow(),
                  _ColoursAndMore(),
                  _ButtonsLikeCartSettings()
                ],
              ),
            ),
          )
        ],
      )
    );
  }
}

class _ButtonsLikeCartSettings extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 30),
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          _ButtonWithShadow(Icon(Icons.star, color: Colors.red, size: 25,)),
          _ButtonWithShadow(Icon(Icons.shopping_cart, color: Colors.grey.withOpacity(0.4), size: 25,)),
          _ButtonWithShadow(Icon(Icons.settings, color: Colors.grey.withOpacity(0.4), size: 25,))
        ],
      ),
    );
  }
}

class _ButtonWithShadow extends StatelessWidget {

  final Icon icon;

  const _ButtonWithShadow(this.icon);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 55,
      height: 55,
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(color: Colors.black, spreadRadius: -5, blurRadius: 20, offset: Offset(0,10))
        ]
      ),
      child: this.icon,
    );
  }
}

class _ColoursAndMore extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 10),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Stack(
              children: <Widget>[
                Positioned( left: 90, child: _ColorButton( Color(0xffC6D642), 4, 'assets/imgs/verde.png')),
                Positioned( left: 60, child: _ColorButton( Color(0xffFFAD29), 3, 'assets/imgs/amarillo.png')),
                Positioned( left: 30, child: _ColorButton( Color(0xff2099F1), 2, 'assets/imgs/azul.png')),
                _ColorButton( Color(0xff364D56), 1, 'assets/imgs/negro.png'),
              ],
            ),
          ),
          OrangeButton(text: 'More related items', height: 30, width: 170, color: Color(0xffFFC675))
        ],
      ),
    );
  }
}

class _ColorButton extends StatelessWidget {

  final Color color;
  final int index;
  final String imageUrl;

  const _ColorButton(this.color, this.index, this.imageUrl);

  @override
  Widget build(BuildContext context) {
    return FadeInLeft(
      delay: Duration(milliseconds: this.index * 100),
      duration: Duration(milliseconds: 300),
      child: GestureDetector(
        onTap: () {
          Provider.of<ShoeModel>(context, listen: false).assetImage = this.imageUrl;
        },
        child: Container(
          width: 45,
          height: 45,
          decoration: BoxDecoration(
            color: this.color,
            shape: BoxShape.circle
          ),
        ),
      ),
    );
  }
}

class _AmmountBuyNow extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 30),
      child: Container(
        margin: EdgeInsets.only(top: 20, bottom: 20),
        child: Row(
          children: <Widget>[
            Text('\$180.0', style: TextStyle(fontSize: 28, fontWeight: FontWeight.bold)),
            Spacer(),
            Bounce(
              delay: Duration(seconds: 1),
              from: 8,
              child: OrangeButton(text: 'Buy now', width: 120, height: 40,)
            ),
          ],
        ),
      ),
    );
  }
}